
import os 
from perspectiveInverter import *
import cv2 as cv
import numpy as np

class PixelPoints:
    def __init__(self,converter):
        self.converter = converter


    def pickPointPixels(self,event,x,y,flags,param):
        if event == cv.EVENT_LBUTTONDOWN:
            print(f'image Point: {x}, {y}')
            realCoord = self.converter.from2Dto3D(np.array([[x],[y],[1]], np.float32))
            print(f'real world coordinates are: {realCoord}')
            
        
if __name__ == '__main__':
    
    dirname = os.path.dirname(__file__)
    workingDirectory = os.path.join(dirname, '21-05-2021-calibrationv2.0\saved\\')
    
    #Image acquisition

    url ="http://cameraopencv.ddns.net:12000/video" #remote url
    
    
    cap = cv.VideoCapture(url)
    
    ret,img = cap.read()
    
    if not ret:
        print("Can't receive frame (stream end?). Exiting ...")
    converter = PerspectiveInverter(img, workingDirectory, 1)
    pixelFinder = PixelPoints(converter)
    cv.namedWindow('image',cv.WINDOW_NORMAL)
    cv.setWindowProperty('image',cv.WINDOW_NORMAL,cv.WINDOW_NORMAL)
    cv.setMouseCallback('image',pixelFinder.pickPointPixels)
    
    while ret:
        ret ,img = cap.read()
        cv.imshow("image",img)
        
        if cv.waitKey(0) == ord('q') :
            break
    
    cv.destroyAllWindows()
    