import numpy as np
import cv2 as cv

url ="http://192.168.1.26:8080/video"

cap = cv.VideoCapture(url)

name = "calibration"
name = name + '.mp4'
fourcc = cv.VideoWriter_fourcc(*'MP4V')
out = cv.VideoWriter(name, fourcc, 30.0, (1920,1088))

if not cap.isOpened():
    print("Cannot open camera")
    exit()
while True:
    # Capture frame-by-frame
    ret, frame = cap.read()
    
    #frame = cv.resize(frame, (0,0), fx=0.5, fy=0.5)
    # if frame is read correctly ret is True
    if not ret:
        print("Can't receive frame (stream end?). Exiting ...")
        break
    
    # Our operations on the frame come here
    #gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    #cv.imshow('frame', gray)
    
    out.write(frame)
    #Resizing each frame for better view
    frame = cv.resize(frame, (0,0), fx=0.5, fy=0.5)
    # Display the resulting frame
    cv.imshow('frame', frame)
    if cv.waitKey(1) == ord('q'):
        break
# When everything done, release the capture
cap.release()
cv.destroyAllWindows()