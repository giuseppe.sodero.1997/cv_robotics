import numpy as np
import cv2 as cv
import glob

url = "http://192.168.1.26:8080/video"
saving_directory = "C:/Users/andre/Desktop/calibrazioni/calibrazione2/saved"




cap = cv.VideoCapture(url)
if not cap.isOpened():
    print("Cannot open camera")
    exit()
else:
    ret, frame = cap.read()
    # if frame is read correctly ret is True
    if not ret:
        print("Can't receive frame (stream end?). Exiting ...")
        cap.release()
        cv.destroyAllWindows()
        exit()
    # Our operations on the frame come here
    
    # Display the resulting frame
    win_name = 'frame'
    cv.namedWindow(win_name,cv.WINDOW_NORMAL)
    cv.setWindowProperty(win_name,cv.WINDOW_NORMAL,cv.WINDOW_NORMAL)
    
    #[0,0,0] -> 842,960
    #[40,40,0] -> 924,876
    #[160,80,0] -> 1179, 793
    #[40,200,0] -> 924,539
    #[120,160,0]-> 1092,622
    #[200,360,0] -> 1263, 198
    #[120,280,0] -> 1091,370
    #[160,160,0] -> 1178,622
    #[120,360,0] -> 1093, 200
    #[80,240,0] -> 1008, 455
    
    frame = cv.circle(frame, (842,960), radius = 1  , color=(0,0,255),thickness = 1)
    frame = cv.circle(frame, (924,876), radius = 1  , color=(0,0,255),thickness = 1)
    frame = cv.circle(frame, (1179,793), radius = 1  , color=(0,0,255),thickness = 1)
    frame = cv.circle(frame, (925,539), radius = 1  , color=(0,0,255),thickness = 1)
    frame = cv.circle(frame, (1092,622), radius = 1  , color=(0,0,255),thickness = 1)
    frame = cv.circle(frame, (1263,198), radius = 1  , color=(0,0,255),thickness = 1)
    frame = cv.circle(frame, (1091,370), radius = 2  , color=(0,0,255),thickness = 1)
    
    frame = cv.circle(frame, (1178,622), radius = 1  , color=(0,0,255),thickness = 1)
    frame = cv.circle(frame, (1093,200), radius = 1  , color=(0,0,255),thickness = 1)
    frame = cv.circle(frame, (1008, 455), radius = 1 , color=(0,0,255),thickness = 1)
    
    #Pixel for images coordinates to World reference frame
    
    frame = cv.circle(frame, (1263, 538), radius = 10 , color=(0,0,255),thickness = 1)
    
    cv.imshow(win_name, frame)
    while True:
        if cv.waitKey(1) == ord('q'):
            break
        


    