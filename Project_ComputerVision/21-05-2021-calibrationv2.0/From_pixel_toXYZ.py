import numpy as np
import cv2 as cv
from pathlib import Path
class PixelFinder():
    def __init__(self, name, saving_directory):
        self.name = name
        #Parameters setup 
        calibration_number = 1
        camera_matrix = np.load(saving_directory+"camera_matrix"+ str(calibration_number) +".npy")
        self.distortion_coeff = np.load(saving_directory+"distortion_coeff"+ str(calibration_number) +".npy")
        rot_mtxRC0 = np.load(saving_directory+"rot_mtxRC0_"+ str(calibration_number) +".npy")
        self.tRC0 = np.load(saving_directory+"tRC0_"+ str(calibration_number) +".npy")
        self.scaling_factor_average = np.load(saving_directory+"scaling_factor_average_"+str(calibration_number)+".npy")
        self.camera_matrix_inv = np.linalg.inv(camera_matrix)
        self.rot_mtxRC0_inv = np.linalg.inv(rot_mtxRC0)

        self.insert_manually = False
        #Horizontal coordinate
        self.u = 1263    
        #Vertical coordinate
        self.v =538   
        #Inverse matrix
    
    def pickPointPixels(self,event,x,y,flags,param):
        if event == cv.EVENT_LBUTTONDOWN:
            print(x,y)
            if self.insert_manually == True:
                uv1 = np.array([[self.u],[self.v],[1]],dtype=np.float32)
            else:
                uv1 = np.array([[x],[y],[1]],dtype=np.float32)

            #beginnig computation of XYZ in world frame
            suv1 = self.scaling_factor_average*uv1
            #camera_matrix_inv * suv1
            temp1 = self.camera_matrix_inv.dot(suv1) - self.tRC0

            XYZ1 = self.rot_mtxRC0_inv.dot(temp1)
            print("Coordinates Points in WORLD REFERENCE FRAME")
            print(XYZ1)

if __name__ == '__main__':
    
    #Image acquisition

    url ="http://192.168.1.26:8080/video"
    cap = cv.VideoCapture(0)
    
    #saving_directory = "C:/Users/andre/Desktop/calibrazioni/calibrazione2/saved/"
    #saving_directory = str(Path(__file__).parent) + "../saved/"
    saving_directory = 'C:/Users/giuse/Desktop/21-05-2021/saved/'
    
    pointPicker = PixelFinder('First pixel finder', saving_directory)  
    

    cv.namedWindow('image',cv.WINDOW_NORMAL)
    cv.setWindowProperty('image',cv.WINDOW_NORMAL,cv.WINDOW_NORMAL)
    cv.setMouseCallback('image',pointPicker.pickPointPixels)
    

    while True:
        ret,img = cap.read()
        if not ret:
            print("Can't receive frame (stream end?). Exiting ...")
        cv.imshow("image",img)
        
        #INSERT MANUALLY INPUT PIXEL COORDINATES 
        

        
        if cv.waitKey(1) == ord('q') :
            break
    
    cv.destroyAllWindows()









