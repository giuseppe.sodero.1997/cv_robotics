import numpy as np
import cv2 as cv

saving_directory = "C:/Users/andre/Desktop/calibrazioni/calibrazione2/saved/"
point_number = 10
calibration_number = 1
#WorldPoints
worldPoints=np.array([
        [0,0,0],
        [40,40,0],
        [160,80,0],
        [40,200,0],
        [120,160,0],
        [200,360,0],
        [120,280,0],
        
        [180,180,0],
        [120,360,0],
        [80,240,0]
    ], dtype=np.float32)


    #[0,0,0] -> 842,960
    #[40,40,0] -> 924,876
    #[160,80,0] -> 1179, 793
    #[40,200,0] -> 924,539
    #[120,160,0]-> 1092,622
    #[200,360,0] -> 1263, 198
    #[120,280,0] -> 1091,370
    
    #[160,160,0] -> 1178,622
    #[120,360,0] -> 1093, 200
    #[80,240,0] -> 1008, 455

#Pixel Points

pixelPoints = np.array([
        [842,960],
        [924,876],
        [1179,793],
        [924,539],
        [1092,622],
        [1263, 198],
        [1091,370],
        [1178, 622],
        [1093,200],
        [1008,455]
    ], dtype = np.float32)



#Loading values
camera_matrix = np.load(saving_directory + "camera_matrix"+str(calibration_number)+".npy")
distortion_coeff = np.load(saving_directory + "distortion_coeff"+str(calibration_number)+".npy")
rvecs = np.load(saving_directory+"rvecs"+str(calibration_number)+".npy")
tvecs = np.load(saving_directory+"tvecs"+str(calibration_number)+".npy")

#Solving PnP

ret,rvec,tvec, _  = cv.solvePnPRansac(worldPoints,pixelPoints,camera_matrix,distortion_coeff)

print("Rotation Matrix from camera to fixed reference frame C^R_0")

rot_mtx,_ = cv.Rodrigues(rvec)
np.save(saving_directory+"rot_mtxRC0_"+str(calibration_number)+".npy",rot_mtx)
print(rot_mtx)

print("Translation vector from center of the camera to center of fixed frame in the camera frame C^t_C0")
np.save(saving_directory+"tRC0_"+str(calibration_number)+".npy",tvec)
print(tvec)

#Extrinsic Matrix = Rotation matrix + translation vector
Rt = np.column_stack((rot_mtx,tvec))
print("Extrinsic matrix R|t")
print(Rt)
#Projection matrix computation A[R|t]
projection_mtx = camera_matrix.dot(Rt)
print("Projection Matrix A[R|t]")
print(projection_mtx)



#Estimate scaling factor s
print("\n\n-----------START Scaling factor estimation--------\n\n")
scaling_factor_list = np.zeros((1,point_number),dtype=np.float32)

for i in range(0,point_number):
    XYZ1 = np.array([
        [worldPoints[i,0], worldPoints[i,1],worldPoints[i,2],1]
        ],dtype=np.float32)
    suv1 = projection_mtx.dot(XYZ1.T)
    scaling_factor_list[0,i] =suv1[2,0]
print("\n\n###############################\n\n")

print("Scaling factor LIST")
print(scaling_factor_list)

print("\n\n###############################\n\n")
print(f"Calibration number: #{calibration_number}")

print("Scaling Factor average")
scaling_factor_average = np.mean(scaling_factor_list)
np.save(saving_directory+"scaling_factor_average_"+str(calibration_number)+".npy",scaling_factor_average)
print(scaling_factor_average)

print("Scaling Factor standard deviation")
scaling_factor_deviation = np.std(scaling_factor_list)
print(scaling_factor_deviation)

print("\n\n###############################\n\n")

for i in range(point_number):
    print(f"Point #{i} ")
    print(f"s:{scaling_factor_list[0,i]}")
    print(f"Error: {scaling_factor_list[0,i]-scaling_factor_average}")

print("\n\n-----------END Scaling factor estimation--------\n\n")

print("\n\n###############################\n\n")