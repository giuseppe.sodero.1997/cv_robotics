import numpy as np 
import cv2 as cv 
from perspectiveInverter import *
import os
class ObjectDetectionByColor():
    def __init__(self, HSVlow = np.array([110, 90, 40]) , HSVhigh =np.array([[130, 255, 255]]), camera = "http://cameraopencv.ddns.net:12000/video"):
        self.camera = camera
        self.HSVlow = HSVlow 
        self.HSVhigh = HSVhigh
        dirname = os.path.dirname(__file__)
        workingDirectory = os.path.join(dirname, '21-05-2021-calibrationv2.0\saved\\')
        #Font per stampa numeri su immagine
        font = cv.FONT_HERSHEY_COMPLEX
        #Image acquisition
        cap = cv.VideoCapture(camera)
        ret, imag = cap.read()
        self.converter = PerspectiveInverter(imag, workingDirectory, 1)

    def getPose(self):
        cap = cv.VideoCapture(self.camera)
        ret, imag = cap.read()
        font = cv.FONT_HERSHEY_COMPLEX
        if not ret:
            print('errore camera non disponibile')
            return None
        hsv = cv.cvtColor(imag, cv.COLOR_BGR2HSV)
        mask = cv.inRange(hsv, self.HSVlow, self.HSVhigh)
        res = cv.bitwise_and(imag, imag, mask=mask)
        contours, hierarchy = cv.findContours(mask, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

        ######################  Generazione delle coordinate del centroide
        
        try:
            cnt = contours[0]
            M = cv.moments(cnt)
            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])
            center = self.converter.from2Dto3D(np.array([[cx], [cy], [1]], np.float32)) 
            string = 'cx =' + str(center[0]) + " " + 'cy =' + str(center[1]) 
            cv.putText(imag, string, (cx+20, cy+100), font, 0.5, (255, 0, 255),2)
            cv.circle(imag,(cx, cy), 3, (0, 0, 255), -1)
            #computing Contours       
            approx = cv.approxPolyDP(cnt, 0.1 * cv.arcLength(cnt, True), True)
            #cv.drawContours(imag, [approx], 0, (0, 0, 255), 4)
            n = approx.ravel() 
            
            #print(f'la dimensione di n è{n.shape}')
            i = 0
            corner = []
            for j in n :
                if(i % 2 == 0):
                    x = n[i]
                    y = n[i + 1]
                    actual_corner = self.converter.from2Dto3D(np.array([[x], [y], [1]], np.float32))
                    corner.append(actual_corner)
                    string = 'x =' + str(actual_corner[0]) + " " + 'y =' + str(actual_corner[1])
                    cv.putText(imag, string, (x, y), font, 0.5, (255, 255, 255))
                print(f"Lo stramaledetto i vale {i}")
                
                i = i + 1
            print(f"Corner Length {len(corner)}")
            print(f"Corner: {corner}")
            
            """ 
            #Alternativa al ciclo for di sopra
            N = n.shape[0]
            corner2 = []
            for i in range(int(N/2)):
                pippo_x = n[i]
                pippo_y = n[i+1]     
                actual_corner = self.converter.from2Dto3D(np.array([[pippo_x], [pippo_y], [1]], np.float32))
                corner2.append(actual_corner)
            print(f"corner2: {corner2}")
            """
            
            self.converter.draw_world_reference_frame(imag)
            self.converter.draw_object_reference_frame(n,imag)
            cv.namedWindow('Originale',cv.WINDOW_NORMAL)
            cv.setWindowProperty('Originale',cv.WINDOW_NORMAL,cv.WINDOW_NORMAL) 
            cv.imshow('Originale',imag)
            if cv.waitKey(50) == ord('q'):
                cv.destroyAllWindows()
            sidesLength = []
            #print(f"le dimensioni di corners è {corner[0].shape}")
            i = 0
            for j in corner:
                if i == len(corner)-1:
                    break
                sidesLength.append(np.sqrt((corner[i][0]- corner[i+1][0])**2 + (corner[i][1]- corner[i+1][1])**2))
                i+=1
                
            print(f"La lunghezza dei lati è {sidesLength}")
            center = center.T
            print(f"Le coordinate dei vertici sono:\n{corner}")
            
            
            #Scalar product computation
            #Per prima cosa ricavo il vettore x1 associato all'oggetto
            x1 = corner[3][0]-corner[0][0]
            y1 = corner[3][1]-corner[0][1]
            
            #calcolo il coseno dell'angolo, considerando che il prodotto scalare sarà dato dal vettore appena ottenuto
            #con il versore [1,0,0]
            
            cos_theta_z = x1/np.sqrt(x1**2+y1**2)
            
            print(f"Il coseno di theta z è: {cos_theta_z}")
            
            theta_z_rad = np.arccos(cos_theta_z)
            theta_z = theta_z_rad*(360/(2*np.pi))
            print(f"L'angolo theta z è: {theta_z}")
            
            object_pose = center.tolist()[0]
            object_pose.append(theta_z.tolist()[0])
            print(f"Il vettore posa è: {object_pose}")
            
            if object_pose == None:
                print(f"Prova object pose: {objct_pose}")
                print("Errore")
                
            return object_pose

        except ZeroDivisionError:
            print('zero division')
            #return None
        except IndexError:
            print('index error') 
            #return None
