import numpy as np 
import cv2 as cv 


class CvCustomError(Exception):
    def __init__(self):
        pass


class PerspectiveInverter():
    def __init__(self, startingFrame, workingDirectory, calibration_number):

        ##########setup per il riconoscimento della scacchiera, in futuro può essere sostituito 
        win_name = "Starting Frame"
        cv.namedWindow(win_name, cv.WINDOW_NORMAL)   
        cv.setWindowProperty(win_name,cv.WINDOW_NORMAL, cv.WINDOW_NORMAL)
        criteria = (cv.TERM_CRITERIA_EPS + cv.TermCriteria_MAX_ITER, 30, 0.001)
        objp = np.zeros((5*9, 3), np.float32)
        #coordinate nel mondo reale degli incroci della scacchiera
        squareSize = 40#mm
        objp[:, :2] = np.mgrid[0:9, 0:5].T.reshape(-1, 2)*squareSize
        #setup dell'immagine
        gray = cv.cvtColor(startingFrame, cv.COLOR_BGR2GRAY)
        ret, corners = cv.findChessboardCorners(gray, (9, 5),None)
        try:
            if ret ==True:
                corners2 = cv.cornerSubPix(gray, corners, (11,11), (-1, -1), criteria)
                # stampo il risultato  
                cv.drawChessboardCorners(startingFrame, (9,5), corners2, ret)
                cv.imshow(win_name, startingFrame)
                pixelPoints = corners2
                print(f'vediamo sta cagata {corners2.shape}')
                self.origin = (corners2[0][0][0], corners2[0][0][1])
                self.AxisX = (corners2[1][0][0], corners2[1][0][1])
                self.AxisY = (corners2[9][0][0], corners2[9][0][1])
                self.AxisXtext = (int(corners2[1][0][0]+20), int(corners2[1][0][1]))
                self.AxisYtext = (int(corners2[9][0][0]-40), int(corners2[9][0][1]-20))
                print(f'origin is:{self.origin}')
                print(f'x is:{self.AxisX}')
                print(f'y is: {self.AxisY}')
            else:
                raise CvCustomError()

        except CvCustomError:
            print('Sistema di riferimento non trovato')
        ##########fine setup per il riconoscimento della scacchiera 
        
        #Setup dei parametri della camera  
        self.distortion_coeff = np.load(workingDirectory+"distortion_coeff"+ str(calibration_number) +".npy")
        camera_matrix = np.load(workingDirectory+"camera_matrix"+ str(calibration_number) +".npy")
        ret,rvec,tvec, _  = cv.solvePnPRansac(objp,pixelPoints,camera_matrix,self.distortion_coeff)
        rot_mat, _= cv.Rodrigues(rvec)
        print(f"rvec is : {rot_mat}")
        #Salvo come attributi della classe 
        print(f"tvec is : {tvec}")
        self.tvec = tvec 
        self.rot_mtxRC0_inv = np.linalg.inv(rot_mat)
        self.scaling_factor_average = np.load(workingDirectory+"scaling_factor_average_"+str(calibration_number)+".npy")
        self.camera_matrix_inv = np.linalg.inv(camera_matrix)
    
    def from2Dto3D(self, uv1_npArray):
        #beginnig computation of XYZ in world frame
        suv1 = self.scaling_factor_average*uv1_npArray
        #camera_matrix_inv * suv1

        temp1 = self.camera_matrix_inv.dot(suv1) - self.tvec

        xyz = self.rot_mtxRC0_inv.dot(temp1)
        
        return xyz
    def draw_world_reference_frame(self, imageFrame):
        font = cv.FONT_HERSHEY_COMPLEX
        cv.line(imageFrame, self.origin, self.AxisX, (0,0,255), 4)
        cv.putText(imageFrame, 'x', self.AxisXtext, font, 0.7, (0, 0, 255), 3)
        cv.line(imageFrame, self.origin, self.AxisY, (0,255,0), 4)
        cv.putText(imageFrame, 'y', self.AxisYtext, font, 0.7, (0,255,0), 3)
        cv.circle(imageFrame, self.origin, 2, (255, 0 , 0), -1)
        cv.circle(imageFrame, self.origin, 4, (255, 0 , 0), 2)
        
    
    def draw_object_reference_frame(self,corner_pixel_list,imageFrame):
        font = cv.FONT_HERSHEY_COMPLEX
        origin = (corner_pixel_list[0],corner_pixel_list[1])
        AxisX = (corner_pixel_list[6],corner_pixel_list[7])
        AxisY = (corner_pixel_list[2],corner_pixel_list[3])
        
        cv.line(imageFrame, origin, AxisX, (0,0,255), 4)
        #cv.putText(imageFrame, 'x', (), font, 0.7, (0, 0, 255), 3)
        cv.line(imageFrame, origin, AxisY, (0,255,0), 4)
        #cv.putText(imageFrame, 'y', self.AxisYtext, font, 0.7, (0,255,0), 3)
