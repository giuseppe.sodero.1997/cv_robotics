import cv2 as cv
import os
import numpy as np 
import time 
from perspectiveInverter import *
dirname = os.path.dirname(__file__)
workingDirectory = os.path.join(dirname, '21-05-2021-calibrationv2.0\saved\\')

#Font per stampa numeri su immagine
font = cv.FONT_HERSHEY_COMPLEX

#Image acquisition
url ="http://cameraopencv.ddns.net:12000/video" #remote url
cap = cv.VideoCapture(url)

ret, imag = cap.read()
converter = PerspectiveInverter(imag, workingDirectory, 1)
time.sleep(1)
print("timer partito")
#time.sleep(20)
print('parte2')
time.sleep(1)
#hsv = cv.bilateralFilter(hsv,9,75,75)
lower = np.array([110, 90, 40])
upper = np.array([130, 255, 255])


while True:
    cap = cv.VideoCapture(url)
    ret, imag = cap.read()
    if not ret:
        print('errore camera non disponibile')
        break

    kernel = np.ones((7,7), np.uint8)
    hsv = cv.cvtColor(imag, cv.COLOR_BGR2HSV)



    mask = cv.inRange(hsv, lower, upper)

    #mask1 = cv.morphologyEx(mask, cv.MORPH_OPEN, kernel)

    #mask2 = cv.morphologyEx(mask1, cv.MORPH_CLOSE, kernel)


    res = cv.bitwise_and(imag, imag, mask=mask)
    #res1 = cv.bitwise_and(imag, imag, mask=mask1)
    #res2 = cv.bitwise_and(imag, imag, mask=mask2)


    contours, hierarchy = cv.findContours(mask, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    """
    #Stampo la lista contours con le dimensioni e tipo
    print("#######################################")
    print(type(contours))
    print("#######################################")
    print(len(contours))
    print("#######################################")
    print(contours[0].shape)
    s1,s2,s3 = contours[0].shape

    for i in range(s1):
        imag = cv.circle(imag, (contours[0][i][0][0],contours[0][i][0][1]), radius = 5  , color=(0,0,255),thickness = 1)
    print("#######################################")
    print(contours)
    print("#######################################")
    """
    
    ######################  Generazione delle coordinate del centroide
    
    try:
        cnt = contours[0]
        M = cv.moments(cnt)
        cx = int(M['m10']/M['m00'])
        cy = int(M['m01']/M['m00'])

        center = converter.from2Dto3D(np.array([[cx], [cy], [1]], np.float32))
        string = 'cx =' + str(center[0]) + " " + 'cy =' + str(center[1]) 
        print(string)
        cv.putText(imag, string, (cx+20, cy+100), font, 0.5, (255, 0, 255),2)
        cv.circle(imag,(cx, cy), 3, (0, 0, 255), -1)
        """
        ###### parte non necessaria, solo un test per essere sicuri che stessimo facendo una media 
        xtot = 0
        ytot = 0
        for i in range(s1):
            xtot += cnt[i][0][0]
        first_x = xtot/len(cnt)
        print(f"momento del prim \'ordine:{first_x}")
        for i in range(s1):
            ytot += cnt[i][0][1]
        first_y = ytot/len(cnt)
        print(f'momento del prim \'ordine:{first_y}')
        """
        ###################### Generazione del perimetro del poligono
        # Going through every contours found in the image.
        #approx = cv.approxPolyDP(cnt, 0.009 * cv.arcLength(cnt, True), True) #Original
        approx = cv.approxPolyDP(cnt, 0.1 * cv.arcLength(cnt, True), True)
        # draws boundary of contours.
        cv.drawContours(imag, [approx], 0, (0, 0, 255), 4) 
        #####################  Generazione dei vertici del poligono
        # Used to flatted the array containing
        # the co-ordinates of the vertices.
        n = approx.ravel() 
        i = 0
        for j in n :
            
            if(i % 2 == 0):
                
            
                x = n[i]
                y = n[i + 1]
                corner = converter.from2Dto3D(np.array([[x], [y], [1]], np.float32))
                
                # String containing the co-ordinates.
                string = 'x =' + str(corner[0]) + " " + 'y =' + str(corner[1]) 
                #if(i == 0):
                    ## text on topmost co-ordinate.
                    #cv.putText(imag, "Arrow tip", (x, y),
                    #                font, 0.5, (255, 0, 0)) 
                #else:
                    # text on remaining co-ordinates.
                #cv.putText(imag, string, (x, y), font, 0.5, (255, 255, 255)) 
                #################################cv.putText(res2, string, (x, y), font, 0.5, (255, 255, 255))  
            i = i + 1
        converter.draw_world_reference_frame(imag)

        ########### mostro i risultati 
        cv.namedWindow('Originale',cv.WINDOW_NORMAL)
        #cv.namedWindow('No filter',cv.WINDOW_NORMAL)
        #cv.namedWindow('Morphology open',cv.WINDOW_NORMAL)
        #cv.namedWindow('Morphology open + close',cv.WINDOW_NORMAL)
        cv.setWindowProperty('Originale',cv.WINDOW_NORMAL,cv.WINDOW_NORMAL)
        #cv.setWindowProperty('No filter',cv.WINDOW_NORMAL,cv.WINDOW_NORMAL)
        #cv.setWindowProperty('Morphology open',cv.WINDOW_NORMAL,cv.WINDOW_NORMAL)
        #cv.setWindowProperty('Morphology open + close',cv.WINDOW_NORMAL,cv.WINDOW_NORMAL)
        cv.imshow('Originale',imag)
        #cv.imshow('No filter', res)
        #cv.imshow('Morphology open', res1)
        #cv.imshow('Morphology open + close', res2) 
        if cv.waitKey(50) == ord('q'):
            cv.destroyAllWindows()
            break
    except ZeroDivisionError:
        print('zero division')
    except IndexError:
        print('index error')    
