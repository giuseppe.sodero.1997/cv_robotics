import numpy as np
import cv2 as cv

import glob 


if __name__ == '__main__':
    saving_directory2 = "C:/Users/andre/Desktop/tutorial_openCV/Project-ComputerVision/saved_files2/"
    work_directory = "C:/Users/andre/Desktop/tutorial_openCV/Project-ComputerVision/"
    
    camera_matrix = np.load(saving_directory2 + "camera_matrix.npy")
    distortion_coeff = np.load(saving_directory2 + "distorsion_coeff.npy")
    new_camera_matrix = np.load(saving_directory2 + "newcam_mtx.npy")
    roi = np.load(saving_directory2 + "roi.npy")
    rvecs = np.load(saving_directory2 + "rvecs.npy")
    tvecs = np.load(saving_directory2 + "tvecs.npy")
    
new_image = glob.glob(work_directory +"calibration2/images_for_test_undistorsion/test_undistort.png")
img_new_camera_matrix = cv.imread(new_image[0])

# undistort
undst = cv.undistort(img_new_camera_matrix, camera_matrix, distortion_coeff, None, new_camera_matrix)

# crop the image
x, y, w, h = roi
undst = undst[y:y+h, x:x+w]
#cv.circle(dst,(308,160),5,(0,255,0),2)
win_name1 = "img_new_camera_matrix"
cv.namedWindow(win_name1, cv.WINDOW_NORMAL)   
cv.setWindowProperty(win_name1,cv.WINDOW_NORMAL, cv.WINDOW_NORMAL)

win_name2 = "undistort"
cv.namedWindow(win_name2, cv.WINDOW_NORMAL)   
cv.setWindowProperty(win_name2,cv.WINDOW_NORMAL, cv.WINDOW_NORMAL)

cv.line(img_new_camera_matrix,(1867,1007),(1829,16),(255,0,0),2)
cv.line(img_new_camera_matrix,(97,1023),(1867,1007),(255,0,0),2)
cv.imshow(win_name1, img_new_camera_matrix)

cv.line(undst,(1867,1007),(1829,16),(0,0,255),2)
cv.line(undst,(97,1023),(1867,1007),(0,0,255),2)
cv.imshow(win_name2, undst)

cv.imwrite("undistort_image.png",undst)

while True:
    if cv.waitKey(0) == ord('q'):
        break

cv.destroyAllWindows()

"""
#Check accuracy

mean_error = 0
for i in range(len(objpoints)):
    imgpoints2, _ = cv.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
    error = cv.norm(imgpoints[i], imgpoints2, cv.NORM_L2)/len(imgpoints2)
    mean_error += error
print( "total error: {}".format(mean_error/len(objpoints)) )

"""