import numpy as np
import cv2 as cv
import glob


def save_image(images_list):
    print(len(images_list))
    for i in range(len(images_list)):
        image_name = f'img_{i}.png'
        cv.imwrite(image_name, img)
        
def save_video():
    img_array = []
    for filename in glob.glob(work_directory+'video_from_images/*.png'):
        img = cv.imread(filename)
        height, width, layers = img.shape
        size = (width,height)
        img_array.append(img)

    out = cv.VideoWriter('project.mp4',cv.VideoWriter_fourcc(*'MP4V'), 5, size)

    for i in range(len(img_array)):
        out.write(img_array[i])
    out.release() 
    
    
if __name__ == '__main__':
    
    #Working directory definition
    work_directory = "C:/Users/andre/Desktop/tutorial_openCV/Project-ComputerVision/"
    saving_directory = "C:/Users/andre/Desktop/tutorial_openCV/Project-ComputerVision/19-05-2021-calibration_v1.0/saved_files2"
    
    #Size definition of a square in chessboard 
    square_size = 29 #mm 

    #Definition of the visualization window.
    #With the flag cv.WINDOW_NORMAL the user can change the shape of the window
    #With the flag cv.WND_PROP_FULLSCREEN and cv.WINDOW_FULLSCREEN the window will occupy all the screen during
    #the visualitazion. More at https://docs.opencv.org/3.4/d7/dfc/group__highgui.html#ga66e4a6db4d4e06148bcdfe0d70a5df27
    #and at https://docs.opencv.org/3.4/d7/dfc/group__highgui.html#ga5afdf8410934fd099df85c75b2e0888b
    # ctrl+click on the link to open it.
    win_name = "img"
    cv.namedWindow(win_name, cv.WINDOW_NORMAL)   
    cv.setWindowProperty(win_name,cv.WINDOW_NORMAL, cv.WINDOW_NORMAL)
    
    
    #Termination criteria of the cornerSubPix function
    criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    
    #Preparation object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((4*6,3), np.float32)
    
    #Add the real dimension of the square to the object points
    objp[:,:2] = np.mgrid[0:6,0:4].T.reshape(-1,2)*square_size
    
    
    # Arrays initialization to store object points and image points from all the images.
    objpoints = [] # 3d point in real world space
    imgpoints = [] # 2d points in image plane.
    images = glob.glob(work_directory + "calibration2/selection_calibration2/*.png")


    #Images acquisition and Chessboard corner detection 
    
    for fname in images:
        img = cv.imread(fname)
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        
        # Find the chess board corners
        ret, corners = cv.findChessboardCorners(gray, (6,4), None)
        
        # If found, add object points, image points (after refining them)
        if ret == True:
            objpoints.append(objp)
            corners2 = cv.cornerSubPix(gray,corners, (11,11), (-1,-1), criteria)
            imgpoints.append(corners)
            
            # Draw and display the corners
            cv.drawChessboardCorners(img, (6,4), corners2, ret)
            
            #Displaying images with the found corners
            cv.imshow(win_name, img)
            
            if cv.waitKey(1) == ord('q'):
                break

    cv.destroyAllWindows()
    
    #Camera calibration and intrinsic parameter estimation
    print("-----------Starting calibration-----------")
    ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)
    
    
    print(ret)
    
    
    print("Camera Matrix")
    print(mtx)
    np.save(saving_directory+"camera_matrix.npy",mtx)
    
    print("Distortion Coefficients")
    print(dist)
    np.save(saving_directory+"distortion_coeff.npy",dist)
    
    print("Rotation vectors")
    np.save(saving_directory+"rvecs.npy",rvecs)
    
    print("Translation vector")
    np.save(saving_directory+"tvecs.npy",tvecs)

    print("-----------Calibration Ended-----------")
    
    new_image = glob.glob(work_directory +"images_for_test_undistorsion/test_undistort.png")
    img_new_camera_matrix = cv.imread(new_image[0])
    
    height,width = img_new_camera_matrix.shape[:2]
    
    print("Image Width, Height")
    print(width, height)
    #if using Alpha 0, so we discard the black pixels from the distortion.  this helps make the entire region of interest is the full dimensions of the image (after undistort)
    #if using Alpha 1, we retain the black pixels, and obtain the region of interest as the valid pixels for the matrix.
    #i will use Apha 1, so that I don't have to run undistort.. and can just calculate my real world x,y
    newcam_mtx, roi=cv.getOptimalNewCameraMatrix(mtx, dist, (width,height), 1, (width,height))
    
    
    print("Region of Interest")
    print(roi)
    np.save(saving_directory+'roi.npy', roi)

    print("New Camera Matrix")
    #print(newcam_mtx)
    np.save(saving_directory+'newcam_mtx.npy', newcam_mtx)
    print(np.load(saving_directory+'newcam_mtx.npy'))
    
    # undistort
undst = cv.undistort(img_new_camera_matrix, mtx, dist, None, newcam_mtx)

# crop the image
x, y, w, h = roi
undst = undst[y:y+h, x:x+w]
#cv.circle(dst,(308,160),5,(0,255,0),2)

img_new_camera_matrix = cv.resize(img_new_camera_matrix, (0,0), fx=0.5, fy=0.5)
cv.imshow('img_new_camera_matrix', img_new_camera_matrix)
undst = cv.resize(undst, (0,0), fx=0.5, fy=0.5)
cv.imshow('undistort', undst)

while True:
    if cv.waitKey(0) == ord('q'):
        break

cv.destroyAllWindows()

#Check accuracy

mean_error = 0
for i in range(len(objpoints)):
    imgpoints2, _ = cv.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
    error = cv.norm(imgpoints[i], imgpoints2, cv.NORM_L2)/len(imgpoints2)
    mean_error += error
print( "total error: {}".format(mean_error/len(objpoints)) )