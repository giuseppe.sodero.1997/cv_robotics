import numpy as np
import cv2 as cv
import glob


class PixelPoints:
    def __init__(self,saving_directory):
        self.saving_directory = saving_directory
        self.index = 0
        self.pixelPoints = np.zeros((5,2),dtype=np.float32)


    def pickPointPixels(self,event,x,y,flags,param):
        if event == cv.EVENT_LBUTTONDOWN and self.index < 5:
            self.pixelPoints[self.index,:]=[x,y] 
            print(x,y)
            self.index += 1
        else:
            self.savePixelPoints()
        
        
    def savePixelPoints(self):
        np.save(self.saving_directory + "pixelPoints.npy",self.pixelPoints)


if __name__ == '__main__':
    
    pixelFinder = PixelPoints("C:/Users/andre/Desktop/tutorial_openCV/Project-ComputerVision/saved_files/")
    
    square_size = 29
    
    #Image acquisition
    url ="http://192.168.1.26:8080/video"
    cap = cv.VideoCapture(url)
    
    ret,img = cap.read()
    
    if not ret:
        print("Can't receive frame (stream end?). Exiting ...")

    cv.namedWindow('image',cv.WINDOW_NORMAL)
    cv.setWindowProperty('image',cv.WINDOW_NORMAL,cv.WINDOW_NORMAL)
    cv.setMouseCallback('image',pixelFinder.pickPointPixels)
    
    while True:
        cv.imshow("image",img)
        
        if cv.waitKey(0) == ord('q') :
            break
    
    cv.destroyAllWindows()
    
    
    
    
        
