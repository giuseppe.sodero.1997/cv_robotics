import numpy as np
import cv2 as cv
import glob


class PixelPoints:
    def __init__(self,saving_directory):
        self.pixelPoints = np.zeros((5,2),dtype=np.float32)
        self.camera_matrix = np.load(saving_directory + "camera_matrix.npy")
        self.new_camera_matrix = np.load(saving_directory + "newcam_mtx.npy")
        self.roi = np.load(saving_directory + "roi.npy")
        self.distortion_coeff = np.load(saving_directory + "distortion_coeff.npy")
        self.pixelPoints = np.load(saving_directory + "pixelPoints.npy")


    def pickPointPixels(self,event,x,y,flags,param):
        if event == cv.EVENT_LBUTTONDOWN:
            print(x,y)



if __name__ == '__main__':
    saving_directory = "C:/Users/andre/Desktop/tutorial_openCV/Project-ComputerVision/saved_files/"
    work_directory = "C:/Users/andre/Desktop/tutorial_openCV/Project-ComputerVision/calibration2/images_for_test_undistorsion/undistort_image.png"
    pixelFinder = PixelPoints(saving_directory)
    """
    #Image acquisition from smartphone
    url ="http://192.168.1.26:8080/video"
    cap = cv.VideoCapture(url)
    
    ret,img = cap.read()
    """
    img = cv.imread(work_directory)
    
    #img = cv.undistort(img,pixelFinder.camera_matrix,pixelFinder.distortion_coeff,None,pixelFinder.new_camera_matrix)
    #x,y,w,h = pixelFinder.roi
    #img = img[y:y+h,x:x+w]
    
    #if not ret:
    #    print("Can't receive frame (stream end?). Exiting ...")

    cv.namedWindow('image',cv.WINDOW_NORMAL)
    cv.setWindowProperty('image',cv.WINDOW_NORMAL,cv.WINDOW_NORMAL)
    cv.setMouseCallback('image',pixelFinder.pickPointPixels)
    
    print(pixelFinder.pixelPoints)
    while True:
        cv.imshow("image",img)
        
        if cv.waitKey(0) == ord('q') :
            break
    
    cv.destroyAllWindows()

    
    
        
