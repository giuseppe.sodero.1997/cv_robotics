import numpy as np
import cv2 as cv

saving_directory = "C:/Users/andre/Desktop/tutorial_openCV/Project-ComputerVision/saved_files/"

worldPoints=np.array([
        [112,199,0],
        [354,206,0],
        [296,373,0],
        [208,646,0],
        [319,740,0]
    ], dtype=np.float32)

#Loading values
pixelPoints = np.load(saving_directory + "pixelPoints.npy")
new_camera_matrix = np.load(saving_directory + "newcam_mtx.npy")
distortion_coeff = np.load(saving_directory + "distortion_coeff.npy")
rvecs = np.load(saving_directory+"rvecs.npy")
tvecs = np.load(saving_directory+"tvecs.npy")

#Solving PnP

ret,rvec,tvec = cv.solvePnP(worldPoints,pixelPoints,new_camera_matrix,distortion_coeff)

print("Rotation Matrix from camera to fixed reference frame C^R_0")

rot_max,_ = cv.Rodrigues(rvec)
np.save(saving_directory+"rot_matRC0.npy",rot_max)
print(rot_max)

print("Translation vector from center of the camera to center of fixed frame in the camera frame C^t_C0")
np.save(saving_directory+"tRC0.npy",tvec)
print(tvec)




p6_0 = np.array([95, 91, 0])

p6_C = rot_max.dot(p6_0) + tvec.T



print("Point 6 in camera frame")
print(p6_C)

#print("--------------------")
#Hp6_C = cv.convertPointsToHomogeneous(p6_C)
#print(Hp6_C[0])
#print("--------------------")

#print("R|t - Extrinsic Matrix")
#Rt=np.column_stack((rot_max,tvec))
#print(Rt)


#print("newCamMtx*R|t - Projection Matrix")
#projection_mtx=new_camera_matrix.dot(Rt)
#print(projection_mtx)


suv1 = new_camera_matrix.dot(p6_C.T)

s = suv1[2,0]

uv1 = suv1/s
print("uv1")
print(uv1)

new_camera_mtx_inv = np.linalg.inv(new_camera_matrix)

rot_max_inv = np.linalg.inv(rot_max)

print(suv1.shape)
print(tvec.shape)


pippo = new_camera_mtx_inv.dot(suv1) - tvec

paperino = rot_max_inv.dot(pippo)
print(pippo)
print(paperino)

#Camera_center_pixel_coordinate = np.array([[962],[560],[1]])
p6_pixel_coordinates = np.array([[1658], [805],[1]])
print("P6 pixel coordinates")
print(p6_pixel_coordinates)

A_invSPuv_minus_tvec = new_camera_mtx_inv.dot(p6_pixel_coordinates*s) - tvec

P6world_coordinates = rot_max_inv.dot(A_invSPuv_minus_tvec)

print(P6world_coordinates)



