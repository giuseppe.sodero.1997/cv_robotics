function [x,y,z,theta_z,data_ready] = getPose()
    working_directory = 'C:\Users\andre\Desktop\cv_robotics\Project_ComputerVision\';
    pymod = strcat(working_directory, 'ObjectDetectionByColor');
    
    data_ready = 0;

    %Aggiungo la directory corrente al Python search path
    %py_addpath(working_directory)

    if count(py.sys.path,working_directory) == 0
        insert(py.sys.path,int32(0), working_directory);
    end

    %Ricarico il codice python in matlab in modo che, se il codice python viene
    %modificato, MATLAB esegue la versione pi� aggiornata del modulo python.
    %clear classes;
    m = py.importlib.import_module('ObjectDetectionByColor');
    module = py.importlib.reload(m);

    new_instance =  py.ObjectDetectionByColor.ObjectDetectionByColor();
    object_pose_pylist = new_instance.getPose();
    
    
    if string(class(object_pose_pylist))~= string('py.NoneType')
        %Conversione da lista python in array matlab
        object_pose = cellfun(@double,cell(object_pose_pylist))
        x = object_pose(1);
        y = object_pose(2);
        z = object_pose(3);
        theta_z = object_pose(4);
        data_ready = 1;
    else
        x = 0
        y = 0
        z = 0
        theta_z = 0
        data_ready = 0    
    end

end


